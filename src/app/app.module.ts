import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Chart,ChartModule,HIGHCHARTS_MODULES } from 'angular-highcharts';
import exporting from 'highcharts/modules/exporting.src.js';
import { LineChartComponent } from './line-chart/line-chart.component';
import { StackedBarComponent } from './stacked-bar/stacked-bar.component';
import { StackedColumnComponent } from './stacked-column/stacked-column.component';

export function highchartsModules(){
  return [exporting];
}

@NgModule({
  declarations: [
    AppComponent,
    LineChartComponent,
    StackedBarComponent,
    StackedColumnComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartModule 
  ],
  providers: [{ provide: HIGHCHARTS_MODULES, useFactory: highchartsModules}],
  bootstrap: [AppComponent]
})
export class AppModule { }
