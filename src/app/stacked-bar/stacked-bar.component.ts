import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import * as Highcharts from 'highcharts';
import { Router } from '@angular/router';

@Component({
  selector: 'app-stacked-bar',
  templateUrl: './stacked-bar.component.html',
  styleUrls: ['./stacked-bar.component.scss']
})
export class StackedBarComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

  chart = new Chart({ 
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: ['Clark Street Hospital', 'SpringField General Hospital','Matt"s Hospital', 'St Joseph Hospital','Robin Hospital', 'Forest General Hospital','Epic Verona Hospital', 'Miami Medical']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Case Status Percentage'
        },
        gridLineWidth: 0
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: [{
        type: 'column',
        name: 'Cancelled',
        data: [3, 2,3, 2,3, 2,3, 2]
    }, {
        type: 'column',
        name: 'Referred No Response',
        data: [2, 3,2, 3,2, 3,2, 3]
    }, {
        type: 'column',
        name: 'Declined',
        data: [4, 3,4, 3,4, 3,4, 3]
    }, {
      type: 'column',
      name: 'Accepted but not Booked',
      data: [4, 3,4, 3,4, 3,4, 3]
  }, {
      type: 'column',
      name: 'Other',
      data: [4, 3,4, 3,4, 3,4, 3]
  }]
});

}
