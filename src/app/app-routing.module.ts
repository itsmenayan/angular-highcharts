import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LineChartComponent } from './line-chart/line-chart.component';
import { StackedBarComponent } from './stacked-bar/stacked-bar.component';
import { StackedColumnComponent } from './stacked-column/stacked-column.component';

const routes: Routes = [
  { path: 'lineChart', component: LineChartComponent},
  { path: 'stackedBar', component: StackedBarComponent},
  { path: 'stackedColumn', component: StackedColumnComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
