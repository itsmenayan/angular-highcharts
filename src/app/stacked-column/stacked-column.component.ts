import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import * as Highcharts from 'highcharts';
import { Router } from '@angular/router';

@Component({
  selector: 'app-stacked-column',
  templateUrl: './stacked-column.component.html',
  styleUrls: ['./stacked-column.component.scss']
})
export class StackedColumnComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

  chart = new Chart({
    chart: {
        type: 'column'
       
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: ['Clark Street Hospital', 'SpringField General Hospital','Matt"s Hospital', 'St Joseph Hospital','Robin Hospital', 'Forest General Hospital','Epic Verona Hospital', 'Miami Medical']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Case Status Percentage'
        },
        gridLineWidth: 0
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
    series: [{
      type: 'column',
      name: 'Cancelled',
      data: [3, 2,3, 2,3, 2,3, 2]
  }, {
      type: 'column',
      name: 'Referred No Response',
      data: [2, 3,2, 3,2, 3,2, 3]
  }, {
      type: 'column',
      name: 'Declined',
      data: [4, 3,4, 3,4, 3,4, 3]
  }, {
    type: 'column',
    name: 'Accepted but not Booked',
    data: [4, 3,4, 3,4, 3,4, 3]
}, {
    type: 'column',
    name: 'Other',
    data: [4, 3,4, 3,4, 3,4, 3]
}],
responsive: {
        rules: [{
            condition: {
                
                
            },
            chartOptions: {
                chart: {
                    
                },
                subtitle: {
                    text: null
                },
                navigator: {
                    enabled: false
                }
            }
        }]
    }
// , {
//       type: 'spline',
//       name: 'Accepted but not Booked',
//       data: [30, 20.67, 30, 60.33, 30.33],
//       marker: {
//           lineWidth: 2,
//           lineColor: Highcharts.getOptions().colors[3],
//           fillColor: 'white'
//       }
//   }]
//   ,
//   responsive: {
//     rules: [{
//         condition: {
//             maxWidth: 50,
//             maxHeight: 50
//         },
//         chartOptions: {
//             chart: {
                
//             },
//             subtitle: {
//                 text: null
//             },
//             navigator: {
//                 enabled: false
//             }
//         }
//     }]
// }
});
}
